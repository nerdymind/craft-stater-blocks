<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(
    '*' => array(
        'omitScriptNameInUrls' => true,
        'enableCsrfProtection' => true,
        'extraAllowedFileExtensions' => 'json',

    ),

	'.dev' => array(
        'devMode' => true,
        'cache' => false,
        'environmentVariables' => array(
            'basePath' => $_SERVER['DOCUMENT_ROOT'] . '/',
            'baseUrl'  => '',
        )
    ),

	'*staging_name*.nerdymind.com' => array(
        'cooldownDuration' => 0,
        'devMode' => false,
        'cache' => true,
        'environmentVariables' => array(
            'basePath' => $_SERVER['DOCUMENT_ROOT'] . '/',
            'baseUrl'  => '',
        ),
        //'loginPath' => '/admin',
        
    ),

);
