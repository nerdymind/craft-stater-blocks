## Use this file to initialize the starter "blocks" for Craft CMS
This requires the [FieldManager](https://github.com/engram-design/FieldManager) plugin. Please download and install the plugin first, then use the import functionality to import this repositories **export.json** file.

## Copy files in `config` to the craft `config` folder.
